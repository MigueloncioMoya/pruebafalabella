
module.exports = app => {
  app.context.api.configuracion = {};

  function parse(data) {
    return data._source;
  }
  /**
	 * Metodo que crea un json con la informacion del usuario
	 * @param {json} params Objeto json que representa la información envíada en Request.
	 * @return {json} Objeto json que contiene información de respuesta
	 */

  async function ventaProductos(nombreproducto) {
    console.log("llego a venta productos--------------");
    try {
      let response = await app.context.api.repository.ventaProductos(nombreproducto);
      response = parse(response);
      return { body: response, status: 200 };
    } catch (error) {
      console.log("caigo en el catch ---------",error);
      return { status: 404, body: error };
    }
  }
  async function listarProductos() {
    console.log("llego a listarProductos--------------");
    try {
      let response = await app.context.api.repository.listarProductos();
      console.log("response en controlador --------", response);
      return { body: response, status: 200 };
    } catch (error) {
      console.log("caigo en el catch ---------",error);
      return { status: 404, body: error };
    }
  }
  async function comportamientoProductos(dias) {
    console.log("llego a comportamientoProductos--------------");
    try {
      let response = await app.context.api.repository.comportamientoProductos(dias);
      return { body: response, status: 200 };
    } catch (error) {
      console.log("caigo en el catch ---------",error);
      return { status: 404, body: error };
    }
  }

  app.context.api.configuracion = {
    ventaProductos,
    listarProductos,
    comportamientoProductos
  };
};
