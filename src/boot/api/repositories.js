
module.exports = app => {
  app.context.api.repository = {};
  const {config } = app.context;
  const moment = require('moment');
  const { PRODUCTOS } = config;
  const fs = require('fs');

  /**
	 * Metodo que valida version de la app.
	 * @param {json} Objeto json que solo necesita tener la version de la app
	 */
  async function ventaProductos(nombreproducto) {
    return new Promise((resolve, reject) => {
  (async() => {
    var definicionProd = await app.context.api.modelo.DefinicionProductos(nombreproducto);
     if (definicionProd.nombre == nombreproducto) {
      var producto = new app.context.api.modelo.Productos(definicionProd.nombre, definicionProd.sellin, definicionProd.price);
          fs.readFile('./test.txt', function read(err, data) {
         if (err) {
             return reject(new Error(''));
         }
         if (data.length == 0) {
           var insertOb = [{
             nombre: producto.nombre,
             sellin: producto.sellin,
             price: producto.price
           }];
           fs.writeFile("./test.txt",JSON.stringify(insertOb), function (err) {
              return resolve("ok")
           if (err) {
               return reject(new Error(''));
           }
       });
         }else {
           content = JSON.parse(data);
           var insertOb = {
             nombre: producto.nombre,
             sellin: producto.sellin,
             price: producto.price
           }
            content.push(insertOb);
            fs.writeFile("./test.txt",JSON.stringify(content), function (err) {
               return resolve("ok")
            if (err) {
                return reject(new Error(''));
            }
        });
         }
      });
    }else{
      console.log("producto no encontrado");
      return reject(new Error(''));
    }
  })().catch(e => {
    console.log("error repository -----------"+ e);
    return reject(new Error(''));
  });
});
  }

  async function listarProductos() {
    return new Promise((resolve, reject) => {
  (async() => {
          fs.readFile('./test.txt', function read(err, data) {
         if (err) {
             return reject(new Error(''));
         }
         if (data.length == 0) {
              return resolve("no hay productos para listar")
         }else {
           content = JSON.parse(data);
               return resolve(content)
            if (err) {
                return reject(new Error(''));
            }
         }
      });
      })().catch(e => {
    console.log("error repository -----------"+ e);
    return reject(new Error(''));
      });
    });
  }

  async function comportamientoProductos(dias) {
    return new Promise((resolve, reject) => {
  (async() => {
          var respuestaFinal = [];
          fs.readFile('./test.txt', async function read(err, data) {
         if (err) {
             return reject(new Error(''));
         }
         if (data.length == 0) {
              return resolve("no hay productos para listar")
         }else {
           content = JSON.parse(data);
           for (var i = 0; i < content.length; i++) {
             var producto = new app.context.api.modelo.Productos(content[i].nombre, content[i].sellin, content[i].price);
             for (var j = 0; j < dias; j++) {
               var comportamiento = await producto.comportamiento(j);
               respuestaFinal.push(comportamiento);
             }
           }
           for (var i = 0; i < dias; i++) {
             console.log("-----------dia " + i + "------------");
             console.log("nombre, sellIn, Price");
             for (var j = 0; j < respuestaFinal.length; j++) {
              // console.log("dias -------" + respuestaFinal[j].dia);
               if (respuestaFinal[j].dia == i) {
                 console.log(respuestaFinal[j].nombre+", "+respuestaFinal[j].sellin+", "+respuestaFinal[j].price);
               }
             }
           }
               return resolve(respuestaFinal)
            if (err) {
                return reject(new Error(''));
            }
         }
      });
      })().catch(e => {
    console.log("error repository -----------"+ e);
    return reject(new Error(''));
      });
    });
  }

  app.context.api.repository = {
    ventaProductos,
    listarProductos,
    comportamientoProductos
  };
};
