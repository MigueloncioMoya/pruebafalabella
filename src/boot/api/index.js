const repositories = require('./repositories');
const configuracion = require('./productos');
const modelo = require('../model/productos');


module.exports = app => {
  app.context.api = {};
  repositories(app);
  configuracion(app);
  modelo(app);
};
