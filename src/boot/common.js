module.exports = app => {
  let options = {
    allowBooleanAttributes: true,
    attrNodeName: false,
    attributeNamePrefix: '',
    cdataTagName: false,
    decodeHTMLchar: false,
    ignoreAttributes: false,
    ignoreNameSpace: true,
    localeRange: '',
    parseAttributeValue: undefined,
    parseNodeValue: true,
    trimValues: true
  };
};
