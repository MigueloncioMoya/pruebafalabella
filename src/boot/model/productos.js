module.exports = app => {
  app.context.api.modelo = {};
  const {config } = app.context;

  /**
	 * Metodo que crea un json con la informacion del usuario
	 * @param {json} params Objeto json que representa la información envíada en Request.
	 * @return {json} Objeto json que contiene información de respuesta
	 */
   function Productos(nombre, sellin, price) {
     this.sellin = sellin;
     this.price = price;
     this.nombre = nombre;
   }

   Productos.prototype.comportamiento = async function(diasfaltantes) {
       return new Promise((resolve, reject) => {
       (async() => {
         var arrayRespuesta =[];
         var jsonRespuesta = {}
         var sellin = this.sellin;
         var price = this.price;

     if (this.nombre == "full cobertura") {
         if (sellin - diasfaltantes  < 0) {
            this.price = this.price + 2 >= 100 ? 100 : this.price+ 2;
         }else {
           if (diasfaltantes - sellin  == sellin){
         }else{
             this.price = this.price >= 100 ? 100 : this.price+1;
           }
         }
     }
     if (this.nombre == "mega cobertura") {
     }
     if (this.nombre == "full cobertura super duper") {
       if (sellin - diasfaltantes  < 0) {
          this.price = 0;
       }else {
         if (sellin - diasfaltantes <= 10 && sellin - diasfaltantes >= 6) {
           this.price = this.price + 2
        } else if (sellin - diasfaltantes <= 5) {
           this.price = this.price + 3
           }
         else{
           this.price = this.price + 1;
         }
       }
     }
     if (this.nombre == "super avance") {
       if (sellin - diasfaltantes  < 0) {
          price  = price  >= 0 ? 0 : price  - 2;
       }else {
         if (diasfaltantes == 0) {
         }else{
           price  = price  - 1;
         }
       }
     }
     jsonRespuesta.nombre = this.nombre
     jsonRespuesta.sellin = sellin - diasfaltantes ;
     jsonRespuesta.price = price ;
     jsonRespuesta.dia = diasfaltantes;

   return resolve(jsonRespuesta)
 })().catch(e => {
console.log("error repository -----------"+ e);
return reject(new Error(''));
 });
   });
};

function DefinicionProductos(nombreproducto){
  var retorno = {};
  var definicionproductos = [
    {
    nombre: 'full cobertura',
    sellin: 2,
    price: 0
  },
    {
    nombre: 'mega cobertura',
    sellin: 0,
    price: 180
  },
    {
    nombre: 'full cobertura super duper',
    sellin: 15,
    price: 20
  },
    {
    nombre: 'super avance',
    sellin: 3,
    price: 6
  }
  ]

  definicionproductos.map(function(nombre){
    if (nombre.nombre == nombreproducto) {
        retorno  = nombre
    }

  });
  return   retorno
}

  app.context.api.modelo = {
    Productos,
    DefinicionProductos
  };
};
