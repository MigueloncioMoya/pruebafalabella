const koalogger = require('koa-logger');
const config = require('./config');
const api = require('./api');
const nconf = require('nconf');
const common = require('./common');

module.exports = app => {
  if (nconf.get('NODE_ENV') == 'development') {
    app.use(koalogger());
  }

  process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

  config(app);
  common(app);
  api(app);
};
