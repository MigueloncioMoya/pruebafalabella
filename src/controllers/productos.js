module.exports = {
    ventaProductos: async({ response, request, api }) => {
      const result = await api.configuracion.ventaProductos(request.headers.nombreproducto);
      response.status = result.status;
      response.type = 'application/json';
      response.body = result.body;
    },
    listarProductos: async({ response, request, api }) => {
      const result = await api.configuracion.listarProductos();
      response.status = result.status;
      response.type = 'application/json';
      response.body = result.body;
    },
    comportamientoProductos: async({ response, request, api }) => {
      const result = await api.configuracion.comportamientoProductos(request.headers.dias);
      response.status = result.status;
      response.type = 'application/json';
      response.body = result.body;
    }
  };
