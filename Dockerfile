FROM node:carbon-alpine

WORKDIR /app

COPY package*.json ./

RUN npm install --production

COPY . .
CMD ["NODE_ENV=development"]
WORKDIR /app

EXPOSE 3000


CMD ["npm","start"]
